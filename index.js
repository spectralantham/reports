const express = require('express');
var mongoose = require('mongoose');
var List = require("collections/list");
var bodyParser = require('body-parser');
var Schema = mongoose.Schema;
ObjectId = require('mongodb').ObjectID;
const app = new express();
const ObjectID = require('mongodb').ObjectID;
app.use(express.static("public"));
app.set('view engine', 'ejs');
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
const keys = require('./config/keys')
const port=5000;
app.get('/',function(req,res){
  res.send('<html><h1>success</h1></html>')
});
app.listen(port, () => console.log(`app listening at http://localhost:${port}`))

/*
return users
*/
function computeUser(dbo, callback) {
  dbo.collection("all_users").find({}).toArray(function (err, result) {
    if (err) throw err;
    callback(result)
  });
}
function computeTSSPDCLMeterSummary(prefix,dbo,callback){
  dbname = prefix.slice(1,-1) + "_MeterReadingSummary"
  dbo.collection(dbname).find({}).toArray(function (err, result) {
    if (err) throw err;
    callback(result)
  });
}
function computeTSSPDCLMeterReading(prefix,dbo,callback){
  dbname = prefix.slice(1,-1) + "_MeterReading"
  dbo.collection(dbname).find({}).toArray(function (err, result) {
    if (err) throw err;
    callback(result)
  });
}
/*
Table for TSSPDCL_MeterReading
*/
var prefixes=new List([
'"APSPDCL"',
'"BRPL"',
'"BSESR"',
'"BY"',
'"CESC"',
'"CESCL"',
'"CSPDCL"',
'"DHBVN"',
'"MSEDCL"',
'"TECL"',
'"TPDDL"',
'"TPLAHMEDABAD"',
'"TPMUMBAI"',
'"TSECL"',
'"TSNPDCL"',
'"TSSPDCL"'
])
function meterReadingReport(prefix,res){
  var MongoClient = require('mongodb').MongoClient;
var url = keys.mongodb.dbURI;
MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  var dbo = db.db("cycbackend");
  var userdata=[]
  var metersummary=[]
  var finalresult=[]
  var meterreading=[]
  var unique=new Map();
    computeTSSPDCLMeterReading(prefix,dbo, function (result) {
       meterreading = result;
       computeTSSPDCLMeterSummary(prefix,dbo, function (result) {
         metersummary = result;
         var i=j=k=0;
         var meterreadingset = new Set();
         var metersummaryset = new Set();
         var metersummarymap = new Map();

         for (k = 0; k < meterreading.length; k++) {
          meterreadingset.add(meterreading[k].deviceId)
        }
        for (k = 0; k < metersummary.length; k++ ) {
          metersummaryset.add(metersummary[k].deviceId)
          metersummarymap.set(metersummary[k].deviceId,metersummary[k])
        }
        var intersect = new Set();
        for(var x of meterreadingset) if(metersummaryset.has(x)) intersect.add(x);
        // console.log(intersect)

        for (j = 0; j < meterreading.length-1; j++){
          if(meterreading[j]!=undefined){

          if(intersect.has(meterreading[j].deviceId)) {
            metersummrecord = metersummarymap.get(meterreading[j].deviceId)
                 x = { CANumber: metersummrecord.uidNo, uniqueId: metersummrecord.deviceId, TimeSpent: (parseInt(metersummrecord.endTimestamp,10) - parseInt(metersummrecord.startTimestamp,10)), TimeStamp:meterreading[j].timestamp,kwh: meterreading[j].scanValue, kWHtype:meterreading[j].valueType, kwh_simage:meterreading[j].smallImg,kwh_limage:meterreading[j].bigImg,kw: meterreading[j+1].scanValue, kWtype:meterreading[j+1].valueType, kw_simage:meterreading[j+1].smallImg,kw_limage:meterreading[j+1].bigImg }
                finalresult.push(x);
          }
          else {
            x = { TimeStamp:meterreading[j].timestamp,kwh: meterreading[j].scanValue, kWHtype:meterreading[j].valueType, kwh_simage:meterreading[j].smallImg,kwh_limage:meterreading[j].bigImg,kw: meterreading[j+1].scanValue, kWtype:meterreading[j+1].valueType, kw_simage:meterreading[j+1].smallImg,kw_limage:meterreading[j+1].bigImg }
             finalresult.push(x);
          }
        }
        }
 
        for (k = 0; k < metersummary.length; k++){
          if(!intersect.has(metersummary[k].deviceId)){
            x = { CANumber: metersummary[k].uidNo, uniqueId: metersummary[k].deviceId, TimeSpent: (parseInt(metersummary[k].endTimestamp,10) - parseInt(metersummary[k].startTimestamp,10))}
                finalresult.push(x);
          }
        }
          console.log(finalresult.length)
          console.log(intersect.size)
          finalresult.reverse()
         res.render('pages/TSSPCL', { rows: finalresult });
       })
     })
  });
}
app.get('/:id',function(req,res){
  prefix=(JSON.stringify(req.params.id)).toUpperCase();
  console.log(prefix)
 console.log(prefixes.has(prefix));
  if(prefixes.has(prefix))
   meterReadingReport(prefix,res);
  else
  res.send('<h1>Sorry! Wrong Input</h1>')
 });
